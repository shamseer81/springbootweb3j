// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

contract Test
{
    function run(string memory name) public pure returns (string memory)
    {
        //if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked("test")))
        if(strcmp(name, "test"))
        {
            return "ok";
        }
        
        return "not ok";
    }
    
    function strcmp(string memory a, string memory b) internal pure returns (bool)
    {
        if(bytes(a).length != bytes(b).length)
        {
            return false;
        }
        else
        {
            return keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b));
        }
    }
}