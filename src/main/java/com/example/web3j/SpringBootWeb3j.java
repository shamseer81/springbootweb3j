package com.example.web3j;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.RawTransactionManager;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

@SpringBootApplication
public class SpringBootWeb3j
{
	private final static String INFURA_ENDPOINT = "https://ropsten.infura.io/v3/";
	private final static String INFURA_PROJECT_ID = "xxx";
	private final static String INFURA_PROJECT_SECRET = "yyy";
	
	public static void main(String[] args)
	{
		try
		{
			HttpService httpService = new HttpService(INFURA_ENDPOINT + INFURA_PROJECT_ID);
			String auth = new String(":" + INFURA_PROJECT_SECRET);
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
			String authHeader = "Basic " + new String(encodedAuth);
			httpService.addHeader(HttpHeaders.AUTHORIZATION, authHeader);
			
			Web3j web3j = Web3j.build(httpService);
			
			String privateKeyString = "MYPRIVATEKEYSTRING_YESITHASETHER";
			String address = "ethaddress";
			
			Credentials credentials = Credentials.create(privateKeyString);
			
			String contractAddress = "0x8e329a83B96fc80cc00C0acd5efbf78699b2F299";
			
			final BigInteger gasPrice = BigInteger.valueOf(2205000);
			final BigInteger gasLimit = BigInteger.valueOf(14300000);
			final ContractGasProvider gasProvider = new StaticGasProvider(gasPrice, gasLimit);
			
			TransactionManager manager = new RawTransactionManager(web3j, credentials, 200, 500);
			
			final Test contract = Test.load(contractAddress, web3j, manager, gasProvider);
			
			String a = contract.run("xyz").send(); // <-- here be dragons
			System.out.println("run(xyz): " + a);
		}
		catch(Exception e)
		{
			System.out.println("Problem with Web3j: " +  e.getMessage() + "\n");
			e.printStackTrace();
		}
		
		SpringApplication.run(SpringBootWeb3j.class, args);
	}
}